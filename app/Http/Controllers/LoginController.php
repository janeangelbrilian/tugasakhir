<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use App\Models\Company;
// use App\Models\DaftarPerusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function currentAdmin(Request $request){
        $admin = Admin::find($request -> user());
        return response()->json($admin);
    }

    public function currentCompany(Request $request){
        $company = Company::find($request -> user());
        return response()->json($company);
    }

    public function currentUser(Request $request){
        $user = User::find($request -> user());
        return response()->json($user);
    }

   /**
     * login
     *
     * @param  mixed $request
     * @return void
     */
    public function loginAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $admin = Admin::where('email', $request->email)->first();

        if (!$admin || !Hash::check($request->password, $admin->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Login Failed!',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Login Success!',
            'data'    => $admin,
            'token'   => $admin->createToken('authToken')->accessToken    
        ]);
    }

    /**
     * logout
     *
     * @param  mixed $request
     * @return void
     */
    // public function logoutAdmin(Request $request)
    // {
    //     $removeToken = $request->user()->tokens()->delete();

    //     if($removeToken) {
    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Logout Success!',  
    //         ]);
    //     }
    // }

    public function loginUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Login Failed!',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Login Success!',
            'data'    => $user,
            'token'   => $user->createToken('authToken')->accessToken    
        ]);
    }

    public function loginCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $company = Company::where('email', $request->email)->first();

        if (!$company || !Hash::check($request->password, $company->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Login Failed!',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Login Success!',
            'data'    => $company,
            'token'   => $company->createToken('authToken')->accessToken    
        ]);
    }
}
