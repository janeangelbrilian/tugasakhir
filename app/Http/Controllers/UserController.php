<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function index()
    {
        //get data from table posts
        $users = User::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Dinas',
            'data'    => $users
        ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $users = User::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Dinas',
            'data'    => $users
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email'   => 'required',
            'password' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $users = User::findOrFail($id);

        if ($users) {

            //update post
            $users->update([
                'name'     => $request->name,
                'email'   => $request->email,
                'password'   => $request->password,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Dinas Updated',
                'data'    => $users
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Dinas Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $users = User::find($id);

        if ($users) {

            //delete post
            $users->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Dinas Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Dinas Not Found',
        ], 404);
    }
}
