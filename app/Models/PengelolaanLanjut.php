<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengelolaanLanjut extends Model
{
    use HasFactory;
    
    /**
     * filllable
     *
     * @var array
     */
    protected $fillable = [
     'jenis_jasa', 'nama_pihak_ketiga', 'no_izin', 'masa_berlaku', 'kontrak_kerjasama', 'tanggal_mou', 'masa_berlaku_mou' 
    ];
    public function get_company()
    {
        return $this->belongsTo(Company::class, 'id_company', 'id');
    }
}
