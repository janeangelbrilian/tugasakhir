<?php

namespace App\Http\Controllers;

use App\Models\DaftarPerusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DaftarPerusahaanController extends Controller
{
    // public function store(Request $request)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'nib'   => 'required',
    //         'tgl_terbit_nib'   => 'required',
    //         'nama' => 'required',
    //         'badan_hukum'   => 'required',
    //         'surel'   => 'required',
    //         'password'   => 'required',
    //         'telepon' => 'required',
    //         'situs_web'   => 'required',
    //         'address' => 'required',
    //     ]);

    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //save to database
    //     $daftar_perusahaan = DaftarPerusahaan::create([
    //         'nib'     => $request->nib,
    //         'tgl_terbit_nib'   => $request->tgl_terbit_nib,
    //         'nama'   => $request->nama,
    //         'badan_hukum'   => $request->badan_hukum,
    //         'surel'     => $request->surel,
    //         'password'     => $request->password,
    //         'telepon'   => $request->telepon,
    //         'situs_web'     => $request->situs_web,
    //         'address'   => $request->address
    //     ]);

    //     //success save to database
    //     if ($daftar_perusahaan) {

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Daftar Perusahaan Created',
    //             'data'    => $daftar_perusahaan
    //         ], 201);
    //     }

    //     //failed save to database
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Daftar Perusahaan Failed to Save',
    //     ], 409);
    // }

    public function index()
    {
        //get data from table posts
        $daftar_perusahaan = DaftarPerusahaan::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Daftar Perusahaan',
            'data'    => $daftar_perusahaan
        ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $daftar_perusahaan = DaftarPerusahaan::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Daftar Perusahaan',
            'data'    => $daftar_perusahaan
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nib'   => 'required',
            'tgl_terbit_nib'   => 'required',
            'nama' => 'required',
            'badan_hukum'   => 'required',
            'surel'   => 'required',
            'password'   => 'required',
            'telepon' => 'required',
            'situs_web'   => 'required',
            'address' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $daftar_perusahaan = DaftarPerusahaan::findOrFail($id);

        if ($daftar_perusahaan) {

            //update post
            $daftar_perusahaan->update([
                'nib'     => $request->nib,
                'tgl_terbit_nib'   => $request->tgl_terbit_nib,
                'nama'   => $request->nama,
                'badan_hukum'   => $request->badan_hukum,
                'surel'     => $request->surel,
                'password'     => $request->password,
                'telepon'   => $request->telepon,
                'situs_web'     => $request->situs_web,
                'address'   => $request->address
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Perusahaan Updated',
                'data'    => $daftar_perusahaan
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Perusahaan Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $daftar_perusahaan = DaftarPerusahaan::find($id);

        if ($daftar_perusahaan) {

            //delete post
            $daftar_perusahaan->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Perusahaan Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Perusahaan Not Found',
        ], 404);
    }

}
