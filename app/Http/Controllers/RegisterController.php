<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use App\Models\Company;
// use App\Models\DaftarPerusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
   /**
     * register
     *
     * @param  mixed $request
     * @return void
     */
    public function registerAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $admins = Admin::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Register Success!',
            'data'    => $admins  
        ]);
    }


    public function registerDinas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user;

        return response()->json(['success' => $success]);
    }

  

    public function registerCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nib'   => 'required',
            'tgl_terbit_nib'   => 'required',
            // 'nama' => 'required',
            'badan_hukum'   => 'required',
            'email'   => 'required',
            'password'   => 'required',
            'telepon' => 'required',
            'situs_web'   => 'required',
            'address' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $company = Company::create($input);
        $success['token'] =  $company->createToken('nApp')->accessToken;
        $success['name'] =  $company;

        return response()->json(['success' => $success]);
    }
  
}
