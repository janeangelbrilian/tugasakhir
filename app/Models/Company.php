<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Company extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nib', 'tgl_terbit_nib', 'nama', 'badan_hukum', 'email', 'password', 'telepon', 'situs_web', 'address'
    ];
    public function get_pesan()
    {
        return $this->hasMany(Pesan::class, 'id_company', 'id');
    }
    public function get_limbah()
    {
        return $this->hasMany(LimbahDihasilkan::class, 'id_company', 'id');
    }
    public function get_pengelolaan()
    {
        return $this->hasMany(PengelolaanLanjut::class, 'id_company', 'id');
    }
    public function get_ketentuan()
    {
        return $this->hasMany(Ketentuan::class, 'id_company', 'id');
    }
   
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        // 'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
