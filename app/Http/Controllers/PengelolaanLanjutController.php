<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\PengelolaanLanjut;
use Illuminate\Database\QueryException;

class PengelolaanLanjutController extends Controller
{
    protected $status = null;
    protected $error = null;
    protected $data = null;

    public function store(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'id_company' => 'required',
            'jenis_jasa' => 'required',
            'nama_pihak_ketiga'   => 'required',
            'no_izin' => 'required',
            'masa_berlaku'   => 'required',
            'tanggal_mou'   => 'required',
            'masa_berlaku_mou' => 'required',
            'kontrak_kerjasama' => 'mimes:pdf,jpg,png,jpeg|max:15000',
        ]);

        if ($validator->fails()) {
            return response(
                [
                    'status' => "failed",
                    'data' => ["message" => "data salah"],
                    'error' => $validator->errors(),
                ]
            );
        }

        $document = new PengelolaanLanjut();

        $document->id_company = $request->id_company;
        $document->jenis_jasa = $request->jenis_jasa;
        $document->nama_pihak_ketiga = $request->nama_pihak_ketiga;
        $document->no_izin = $request->no_izin;
        $document->masa_berlaku = $request->masa_berlaku;
        $document->tanggal_mou = $request->tanggal_mou;
        $document->masa_berlaku_mou = $request->masa_berlaku_mou;
        if ($request->kontrak_kerjasama && $request->kontrak_kerjasama->isValid()) {
            $file_name = $request->kontrak_kerjasama->getClientOriginalName();
            $request->kontrak_kerjasama->move(public_path('isu'), $file_name);
            $path = $file_name;
            $document->kontrak_kerjasama = $path;
        }
        try {
            $document->save();
            $this->data = $document;
            $this->status = "success";
        } catch (QueryException $e) {
            $this->status = "failed";
            $this->error = $e;
        }
        return response()->json([
            "status" => $this->status,
            "data" => $this->data,
            "error" => $this->error
        ]);
    }

    public function index()
    {
        return PengelolaanLanjut::select(
            "pengelolaan_lanjuts.*",
            "companies.nama as nama"
        )
            ->join('companies', 'pengelolaan_lanjuts.id_company', '=', 'companies.id')
            ->when(request('search'), function ($query) {
                $query->where('companies.nama', 'like', '%' . request('search') . '%');
            })->with('get_company')->paginate(2);

        // return PengelolaanLanjut::when(request ('search'), function($query){
        //     $query->where('id_company', 'like', '%' . request('search') . '%');
        // })->orderBy('id', 'desc')->with('get_company')->paginate(2);
  
        //get data from table posts
        // $pengelolaan_lanjuts = PengelolaanLanjut::with('get_company')->get();

        //make response JSON
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Daftar Pengelolaan Lanjut B3',
        //     'data'    => $pengelolaan_lanjuts
        // ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show()
    {
        //find post by ID
        $pengelolaan_lanjuts = PengelolaanLanjut::with('get_company')->where('id_company', auth()->guard('company-api')->user()->id)->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Daftar Pengelolaan Lanjut',
            'data'    => $pengelolaan_lanjuts
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */

    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'id_company' => 'required',
            'jenis_jasa' => 'required',
            'nama_pihak_ketiga'   => 'required',
            'no_izin' => 'required',
            'masa_berlaku'   => 'required',
            'tanggal_mou'   => 'required',
            'masa_berlaku_mou' => 'required',
            'kontrak_kerjasama' => 'mimes:pdf,jpg,png,jpeg,pdf|max:15000',
        ]);

        //response error validation
        if ($validator->fails()) {
            $error = $validator->errors()->all()[0];
            return response()->json([
                'status' => 'failed',
                'message' => $error,
                'data' => []
            ]);
        }

        $pengelolaan_lanjuts = PengelolaanLanjut::find($id);
        $pengelolaan_lanjuts->id_company = $request->id_company;
        $pengelolaan_lanjuts->jenis_jasa = $request->jenis_jasa;
        $pengelolaan_lanjuts->nama_pihak_ketiga = $request->nama_pihak_ketiga;
        $pengelolaan_lanjuts->no_izin = $request->no_izin;
        $pengelolaan_lanjuts->masa_berlaku = $request->masa_berlaku;
        $pengelolaan_lanjuts->tanggal_mou = $request->tanggal_mou;
        $pengelolaan_lanjuts->masa_berlaku_mou = $request->masa_berlaku_mou;

        if ($request->kontrak_kerjasama && $request->kontrak_kerjasama->isValid()) {
            $file_name = $request->kontrak_kerjasama->getClientOriginalName();
            $request->kontrak_kerjasama->move(public_path('isu'), $file_name);
            $path = $file_name;
            $pengelolaan_lanjuts->kontrak_kerjasama = $path;
        }
        else{
            unset($pengelolaan_lanjuts['kontrak_kerjasama']);
        }

        $pengelolaan_lanjuts->update();
        return response()->json([
            'status' => 'success',
            'data' => $pengelolaan_lanjuts,
            'messagge' => 'data berhasil di update'
        ]);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $pengelolaan_lanjuts = PengelolaanLanjut::find($id);

        if ($pengelolaan_lanjuts) {

            //delete post
            $pengelolaan_lanjuts->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Pengelolaan Lanjut Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Pengelolaan Lanjut Not Found',
        ], 404);
    }

}