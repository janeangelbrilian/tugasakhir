<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengelolaan_lanjuts', function (Blueprint $table) {
            $table->id();
            $table->string('jenis_jasa');
            $table->string('nama_pihak_ketiga');
            $table->string('no_izin');
            $table->string('masa_berlaku');
            $table->string('kontrak_kerjasama');
            $table->string('tanggal_mou');
            $table->string('masa_berlaku_mou');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengelolaan_lanjuts');
    }
};
