<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DaftarPerusahaanController;
use App\Http\Controllers\KetentuanTeknisController;
use App\Http\Controllers\LimbahDihasilkanController;
use App\Http\Controllers\PengelolaanLanjutController;
use App\Http\Controllers\PesanController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PeranPenggunaController;
use App\Http\Controllers\KetentuanController;
use App\Http\Controllers\KodeLimbahController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('auth:admin-api')->get('currentAdmin',[LoginController::class, 'currentAdmin']);
Route::middleware('auth:company-api')->get('currentCompany',[LoginController::class, 'currentCompany']);
Route::middleware('auth:user-api')->get('currentUser',[LoginController::class, 'currentUser']);



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Register

Route::post('/admin/register', [RegisterController::class, 'registerAdmin']);
Route::post('/registerUser', [RegisterController::class, 'registerDinas']);
Route::post('registerCompany', [RegisterController::class, 'registerCompany']);

// Route::post('/company/register', [RegisterController::class, 'registerCompany']);
// // Route::post('/logout', [LoginController::class, 'logout'])->middleware('auth:api');

// Login

Route::post('admin/login',[LoginController::class, 'loginAdmin'])->name('loginAdmin');
Route::post('user/login',[LoginController::class, 'loginUser'])->name('loginUser');
Route::post('company/login',[LoginController::class, 'loginCompany'])->name('loginCompany');
// Route::group( ['prefix' => 'admin','middleware' => ['auth:admin-api','scopes:admin'] ],function(){
//    // authenticated staff routes here 
//     Route::get('dashboard',[LoginController::class, 'adminDashboard']);
 
// });


// USER / DINAS

// Admin
Route::get('/admin', [AdminController::class, 'index']);
Route::get('/admin/{id}', [AdminController::class, 'show']);
Route::post('/admin/{id}', [AdminController::class, 'update']);
Route::delete('/admin/{id}', [AdminController::class, 'destroy']);

// User
Route::get('/user', [UserController::class, 'index']);
Route::get('/user/{id}', [UserController::class, 'show']);
Route::post('/user/{id}', [UserController::class, 'update']);
Route::delete('/user/{id}', [UserController::class, 'destroy']);


Route::get('/ketentuan/all', [KetentuanController::class, 'index']);
// Route::post('/ketentuan', [KetentuanController::class, 'store']);
Route::middleware('auth:company-api')->post('/ketentuan',[KetentuanController::class, 'store']);


Route::middleware('auth:company-api')->get('/profile',[LoginController::class, 'currentCompany']);
// Company
Route::get('/company', [CompanyController::class, 'index']);
Route::get('/company/{id}', [CompanyController::class, 'show']);
Route::post('/company/{id}', [CompanyController::class, 'update']);
Route::delete('/company/{id}', [CompanyController::class, 'destroy']);


// Kode Limbah
Route::get('/kode_limbah', [KodeLimbahController::class, 'index']);
Route::get('/kode_limbah/{id}', [KodeLimbahController::class, 'show']);
Route::post('/kode_limbah', [KodeLimbahController::class, 'store']);
Route::post('/kode_limbah/{id}', [KodeLimbahController::class, 'update']);
Route::delete('/kode_limbah/{id}', [KodeLimbahController::class, 'destroy']);


// Peran Pengguna
Route::get('/peran_pengguna', [PeranPenggunaController::class, 'index']);
Route::get('/peran_pengguna/{id}', [PeranPenggunaController::class, 'show']);
Route::post('/peran_pengguna', [PeranPenggunaController::class, 'store']);
Route::post('/peran_pengguna/{id}', [PeranPenggunaController::class, 'update']);
Route::delete('/peran_pengguna/{id}', [PeranPenggunaController::class, 'destroy']);

// Ketentuan Teknis TPS
Route::get('/ketentuan_teknis', [KetentuanTeknisController::class, 'index']);
Route::get('/ketentuan_teknis/{id}', [KetentuanTeknisController::class, 'show']);
Route::middleware('auth:company-api')->post('/ketentuan_teknis', [KetentuanTeknisController::class, 'store']);
Route::post('/ketentuan_teknis/{id}', [KetentuanTeknisController::class, 'update']);
Route::delete('/ketentuan_teknis/{id}', [KetentuanTeknisController::class, 'destroy']);

// Limbah B3 Dihasilkan
Route::get('/limbah_dihasilkan/all', [LimbahDihasilkanController::class, 'index']);
Route::middleware('auth:company-api')->get('/limbah_dihasilkan',[LimbahDihasilkanController::class, 'show']);
Route::post('/limbah_dihasilkan', [LimbahDihasilkanController::class, 'store']);
Route::post('/limbah_dihasilkan/{id}', [LimbahDihasilkanController::class, 'update']);
Route::delete('/limbah_dihasilkan/{id}', [LimbahDihasilkanController::class, 'destroy']);

// Pengelolaan Lanjut
Route::get('/pengelolaan_lanjut/all', [PengelolaanLanjutController::class, 'index']);
Route::middleware('auth:company-api')->get('/pengelolaan_lanjut',[PengelolaanLanjutController::class, 'show']);
Route::post('/pengelolaan_lanjut', [PengelolaanLanjutController::class, 'store']);
Route::post('/pengelolaan_lanjut/{id}', [PengelolaanLanjutController::class, 'update']);
Route::delete('/pengelolaan_lanjut/{id}', [PengelolaanLanjutController::class, 'destroy']);

// Pesan
Route::get('/pesan', [PesanController::class, 'index']);
Route::get('/pesan/{id}', [PesanController::class, 'show']);
Route::post('/pesan', [PesanController::class, 'store']);
Route::post('/pesan/{id}', [PesanController::class, 'update']);
Route::delete('/pesan/{id}', [PesanController::class, 'destroy']);
