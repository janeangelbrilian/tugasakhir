<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\LimbahDihasilkan;
use Illuminate\Database\QueryException;
use Auth;


class LimbahDihasilkanController extends Controller
{
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'id_company' => 'required',
            'id_code' => 'required',
            'sumber'   => 'required',
            'jenis' => 'required',
            'jml_timbulan'   => 'required',
            'kemasan' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database

        $limbah_dihasilkan = LimbahDihasilkan::create([
            'id_company' => $request->id_company,
            'id_code'   => $request->id_code,
            'sumber'     => $request->sumber,
            'jenis'   => $request->jenis,
            'jml_timbulan'     => $request->jml_timbulan,
            'kemasan'   => $request->kemasan,
        ]);

        //success save to database
        if ($limbah_dihasilkan) {

            return response()->json([
                'success' => true,
                'message' => 'Daftar Limbah B3 dihasilkan Created',
                'data'    => $limbah_dihasilkan
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Daftar Limbah B3 Failed to Save',
        ], 409);
    }

    // public function getUserById(Request $request){

    // }
    public function index(Request $request)
    {

        return LimbahDihasilkan::select(
            "limbah_dihasilkans.*",
            "companies.nama as nama"
        )
            ->join('companies', 'limbah_dihasilkans.id_company', '=', 'companies.id')
            ->when(request('search'), function ($query) {
                $query->where('companies.nama', 'like', '%' . request('search') . '%');
            })->with('get_company')->paginate(2);

        
        // return LimbahDihasilkan::when(request ('search'), function($query){
        //     $query->where('id_company', 'like', '%' . request('search') . '%');
        // })->orderBy('id', 'desc')->with('get_company')->paginate(2);
  
        //get data from table posts
        // $limbah_dihasilkan = LimbahDihasilkan::with('get_company')->with('get_code')->get();
        

        //make response JSON
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Daftar Limbah B3',
        //     'data'    => $limbah_dihasilkan
        // ], 200);
    }

 
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show()
    {

        //find post by ID
        
        // $limbah_dihasilkan = LimbahDihasilkan::with('get_company')->find($request->user());

        
        $limbah_dihasilkan = LimbahDihasilkan::with('get_code')->with('get_company')->where('id_company', auth()->guard('company-api')->user()->id)->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Daftar Limbah B3',
            'data'    => $limbah_dihasilkan
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'id_company' => 'required',
            'id_code' => 'required',
            'sumber'   => 'required',
            'jenis' => 'required',
            'jml_timbulan'   => 'required',
            'kemasan' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $limbah_dihasilkan = LimbahDihasilkan::findOrFail($id);

        if ($limbah_dihasilkan) {

            //update post
            $limbah_dihasilkan->update([
            'id_company' => $request->id_company,
            'id_code'   => $request->id_code,
            'sumber'     => $request->sumber,
            'jenis'   => $request->jenis,
            'jml_timbulan'     => $request->jml_timbulan,
            'kemasan'   => $request->kemasan,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Limbah B3 Updated',
                'data'    => $limbah_dihasilkan
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Limbah B3 Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $limbah_dihasilkan = LimbahDihasilkan::find($id);

        if ($limbah_dihasilkan) {

            //delete post
            $limbah_dihasilkan->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Limbah B3 Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Limbah B3 Not Found',
        ], 404);
    }

}
