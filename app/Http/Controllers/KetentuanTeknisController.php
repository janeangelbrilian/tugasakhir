<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\KetentuanTeknis;
use Illuminate\Database\QueryException;
use DB;
// use Carbon\Carbon;

class KetentuanTeknisController extends Controller
{
    protected $status = null;
    protected $error = null;
    protected $data = null;


     public function index()
    {
        $ketentuan_teknis = KetentuanTeknis::with('get_company')->get();
     

        $success =  $ketentuan_teknis;

        return response()->json([
            'success' => true,
            'message' => 'Ketentuan Teknis',
            'data' => $success
        ]);
    }

    public function show($id)
    {
        //find post by ID
        $ketentuan_teknis = KetentuanTeknis::with('get_company')->findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Ketentuan Teknis',
            'data'    => $ketentuan_teknis
        ], 200);
    }

    // public function getData(){
    //     if(data){
    //         Liat data dari ketentuan
    //     }else{
    //         Null
    //     }
    // }

    public function store(Request $request){
        
        // dd($request->user());
        
        $data = $request->all();

        $validator = Validator::make($data, [
            // 'latitude' => 'required',
            // 'longitude' => 'required',
            'papan_nama' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'simbol_dan_label' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'pemisah' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'ventilasi' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'pallet' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'penerangan' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'apar' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'log_book' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'sop_penyimpanan' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'sop_tanggap' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'p3k' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'saluran' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'open_dumping' => 'mimes:pdf,jpg,png,jpeg|max:15000',
        ]);

        if ($validator->fails()){
            return response(
                [
                    'status' => "failed",
                    'data' => ["message" => "data salah"],
                    'error' => $validator->errors(),
                ]
                );
        }
        

        // $date = Carbon::now()->toDateString();
        $document = KetentuanTeknis::where('id_company',auth()->user()->id)->first();
        if(is_null($document)){
            $document = new KetentuanTeknis();
        }
        
        if($request->has('papan_nama_tidak')){
            $document->papan_nama = null;
        }

        $document->id_company = auth()->user()->id;
        if($request->has('latitude')){
            $document->latitude = $request->latitude;
        }

        if($request->has('longitude')){
            $document->longitude = $request->longitude;
        }

        
        if($request->has('papan_nama')){
            if ($request->papan_nama && $request->papan_nama->isValid()) {
                $file_name = $request->papan_nama->getClientOriginalName();
                $request->papan_nama->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->papan_nama = $path;
            } 
        }

        if($request->has('simbol')){
            if ($request->simbol && $request->simbol->isValid()) {
                $file_name = $request->simbol->getClientOriginalName();
                $request->simbol->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->simbol = $path;
            } 
        }
        
        if($request->has('pemisah')){
            if ($request->pemisah && $request->pemisah->isValid()) {
                $file_name = $request->pemisah->getClientOriginalName();
                $request->pemisah->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->pemisah = $path;
            } 
        }

        if($request->has('ventilasi')){
            if ($request->ventilasi && $request->ventilasi->isValid()) {
                $file_name = $request->ventilasi->getClientOriginalName();
                $request->ventilasi->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->ventilasi = $path;
            } 
        }

        if($request->has('pallet')){
            if ($request->pallet && $request->pallet->isValid()) {
                $file_name = $request->pallet->getClientOriginalName();
                $request->pallet->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->pallet = $path;
            } 
        }
    
        if($request->has('penerangan')){
            if ($request->penerangan && $request->penerangan->isValid()) {
                $file_name = $request->penerangan->getClientOriginalName();
                $request->penerangan->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->penerangan = $path;
            } 
        }

        if($request->has('apar')){
            if ($request->apar && $request->apar->isValid()) {
                $file_name = $request->apar->getClientOriginalName();
                $request->apar->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->apar = $path;
            } 
        }

        if($request->has('log_book')){
            if ($request->log_book && $request->log_book->isValid()) {
                $file_name = $request->log_book->getClientOriginalName();
                $request->log_book->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->log_book = $path;
            } 
        }

        if($request->has('sop_penyimpanan')){
            if ($request->sop_penyimpanan && $request->sop_penyimpanan->isValid()) {
                $file_name = $request->sop_penyimpanan->getClientOriginalName();
                $request->sop_penyimpanan->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->sop_penyimpanan = $path;
            } 
        }

        if($request->has('sop_tanggap')){
            if ($request->sop_tanggap && $request->sop_tanggap->isValid()) {
                $file_name = $request->sop_tanggap->getClientOriginalName();
                $request->sop_tanggap->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->sop_tanggap = $path;
            } 
        }

        if($request->has('p3k')){
            if ($request->p3k && $request->p3k->isValid()) {
                $file_name = $request->p3k->getClientOriginalName();
                $request->p3k->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->p3k = $path;
            } 
        }

        if($request->has('saluran')){
            if ($request->saluran && $request->saluran->isValid()) {
                $file_name = $request->saluran->getClientOriginalName();
                $request->saluran->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->saluran = $path;
            } 
        }

        if($request->has('open_dumping')){
            if ($request->open_dumping && $request->open_dumping->isValid()) {
                $file_name = $request->open_dumping->getClientOriginalName();
                $request->open_dumping->move(public_path('proposal'), $file_name);
                $path = $file_name;
                $document->open_dumping = $path;
            } 
        }

    DB::beginTransaction();
    try {
        $document->save();
        $this->data = $document;
        $this->status = "success";
        DB::commit();
    } catch (QueryException $e) {
        DB::rollBack();
        $this->status = "failed";
        $this->error  = $e;
    }
    return response()->json([
        "status" => $this->status,
        "data" => $this->data,
        "error" =>$this->error
    ]);
    }

    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'id_company' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'papan_nama' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'simbol_dan_label' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'pemisah' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'ventilasi' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'pallet' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'penerangan' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'apar' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'log_book' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'sop_penyimpanan' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'sop_tanggap' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'p3k' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'saluran' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
            'open_dumping' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
        ]);
        if ($validator->fails()) {
            $error = $validator->errors()->all()[0];
            return response()->json([
                'status' => 'failed',
                'message' => $error,
                'data' => []
            ]);
        }
        $document =  KetentuanTeknis::find($id);
        $document = new KetentuanTeknis();
        $document->id_company = $request->id_company;
        $document->latitude = $request->latitude;
        $document->longitude = $request->longitude;
      
        if ($request->papan_nama && $request->papan_nama->isValid()) {
            $file_name = $request->papan_nama->getClientOriginalName();
            $request->papan_nama->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->papan_nama = $path;
        } 
        if ($request->simbol_dan_label && $request->simbol_dan_label->isValid()) {
            $file_name = $request->simbol_dan_label->getClientOriginalName();
            $request->simbol_dan_label->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->simbol_dan_label = $path;
        } 
        if ($request->pemisah && $request->pemisah->isValid()) {
            $file_name = $request->pemisah->getClientOriginalName();
            $request->pemisah->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->pemisah = $path;
        } 
        if ($request->ventilasi && $request->ventilasi->isValid()) {
            $file_name = $request->ventilasi->getClientOriginalName();
            $request->ventilasi->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->ventilasi = $path;
        } 
        if ($request->pallet && $request->pallet->isValid()) {
            $file_name = $request->pallet->getClientOriginalName();
            $request->pallet->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->pallet = $path;
        } 
        if ($request->penerangan && $request->penerangan->isValid()) {
            $file_name = $request->penerangan->getClientOriginalName();
            $request->penerangan->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->penerangan = $path;
        } 
        if ($request->apar && $request->apar->isValid()) {
            $file_name = $request->apar->getClientOriginalName();
            $request->apar->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->apar = $path;
        } 
        if ($request->log_book && $request->log_book->isValid()) {
            $file_name = $request->log_book->getClientOriginalName();
            $request->log_book->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->log_book = $path;
        } 
        if ($request->sop_penyimpanan && $request->sop_penyimpanan->isValid()) {
            $file_name = $request->sop_penyimpanan->getClientOriginalName();
            $request->sop_penyimpanan->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->sop_penyimpanan = $path;
        } 
        if ($request->sop_tanggap && $request->sop_tanggap->isValid()) {
            $file_name = $request->sop_tanggap->getClientOriginalName();
            $request->sop_tanggap->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->sop_tanggap = $path;
        } 
        if ($request->p3k && $request->p3k->isValid()) {
            $file_name = $request->p3k->getClientOriginalName();
            $request->p3k->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->p3k = $path;
        } 
        if ($request->saluran && $request->saluran->isValid()) {
            $file_name = $request->saluran->getClientOriginalName();
            $request->saluran->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->saluran = $path;
        } 
        if ($request->open_dumping && $request->open_dumping->isValid()) {
            $file_name = $request->open_dumping->getClientOriginalName();
            $request->open_dumping->move(public_path('proposal'), $file_name);
            $path = $file_name;
            $document->open_dumping = $path;
        }
        $document->update();
        return response()->json([
            'status' => 'success',
            'data' => $document,
            'messagge' => 'data berhasil di update'
        ]);
    }

    public function destroy($id){
        $document = KetentuanTeknis::where('id', $id);
        $document->delete();

        return response([
            'status' => "success",
            'data' => ["message" => "data berhasil di hapus"],
            'error' => '' 
        ]);
    }
}
    // public function store(Request $request)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'latitude' => 'required',
    //         'longitude' => 'required',
    //         'papan_nama' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'simbol_dan_label' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'pemisah' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'ventilasi' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'pallet' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'penerangan' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'apar' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'log_book' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'sop_penyimpanan' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'sop_tanggap' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'p3k' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'saluran' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //         'open_dumping' => 'required|mimes:pdf,jpg,png,jpeg|max:15000',
    //     ]);

    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //save to database
    //     $ketentuan_teknis = KetentuanTeknis::create([
    //         'nib'     => $request->nib,
    //         'tgl_terbit_nib'   => $request->tgl_terbit_nib,
    //         'nama'   => $request->nama,
    //         'badan_hukum'   => $request->badan_hukum,
    //         'surel'     => $request->surel,
    //         'telepon'   => $request->telepon,
    //         'situs_web'     => $request->situs_web,
    //         'address'   => $request->address
    //     ]);

    //     //success save to database
    //     if ($daftar_perusahaan) {

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Daftar Perusahaan Created',
    //             'data'    => $daftar_perusahaan
    //         ], 201);
    //     }

    //     //failed save to database
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Daftar Perusahaan Failed to Save',
    //     ], 409);
    // }

    
