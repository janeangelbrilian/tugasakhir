<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LimbahDihasilkan extends Model
{
    use HasFactory;
    
    /**
     * filllable
     *
     * @var array
     */
    protected $fillable = [
    'sumber', 'jenis', 'jml_timbulan', 'kemasan', 'id_company', 'id_code'
    ];
    public function get_company()
    {
        return $this->belongsTo(Company::class, 'id_company', 'id');
    }
    public function get_code()
    {
        return $this->belongsTo(KodeLimbah::class, 'id_code', 'id');
    }
}
