<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    use HasFactory;
    
    /**
     * filllable
     *
     * @var array
     */
    protected $fillable = [
     'judul', 'pesan', 'tanggal','id_company'
    ];
    public function get_company()
    {
        return $this->belongsTo(Company::class, 'id_company', 'id');
    }
}
