<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodeLimbah extends Model
{
    use HasFactory;
    
    /**
     * filllable
     *
     * @var array
     */
    protected $fillable = [
    'name','kode',
    ];

    public function get_limbah()
    {
        return $this->hasMany(LimbahDihasilkan::class, 'id_code', 'id');
    }
}
