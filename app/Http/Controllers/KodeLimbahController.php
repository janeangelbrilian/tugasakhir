<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KodeLimbah;
use Illuminate\Support\Facades\Validator;


class KodeLimbahController extends Controller
{
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'kode'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $kode_limbah = KodeLimbah::create([
            'name'     => $request->name,
            'kode'   => $request->kode,
        ]);

        //success save to database
        if ($kode_limbah) {

            return response()->json([
                'success' => true,
                'message' => 'Daftar Kode Limbah Created',
                'data'    => $kode_limbah
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Daftar Kode Limbah Failed to Save',
        ], 409);
    }

    public function index()
    {
        //get data from table posts
        $kode_limbah = KodeLimbah::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Kode Limbah ',
            'data'    => $kode_limbah
        ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $kode_limbah = KodeLimbah::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Kode Limbah Pengguna',
            'data'    => $kode_limbah
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'kode'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $kode_limbah = KodeLimbah::findOrFail($id);

        if ($kode_limbah) {

            //update post
            $kode_limbah->update([
                'name'     => $request->name,
                'kode'   => $request->kode,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Kode Limbah Updated',
                'data'    => $kode_limbah
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Kode Limbah Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $kode_limbah = KodeLimbah::find($id);

        if ($kode_limbah) {

            //delete post
            $kode_limbah->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Kode Limbah Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Kode Limbah Not Found',
        ], 404);
    }
}
