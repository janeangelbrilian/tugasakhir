<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaftarPerusahaan extends Model
{
    use HasFactory;
    
    /**
     * filllable
     *
     * @var array
     */ 
    protected $fillable = [
        'nib', 'tgl_terbit_nib', 'nama', 'badan_hukum', 'email', 'password', 'telepon', 'situs_web', 'address'
    ];
  
}
