<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ketentuan_teknis', function (Blueprint $table) {
            $table->id();
            $table->string('papan_nama');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('simbol_dan_label');
            $table->string('pemisah');
            $table->string('ventilasi');
            $table->string('pallet');
            $table->string('penerangan');
            $table->string('apar');
            $table->string('log_book');
            $table->string('sop_penyimpanan');
            $table->string('sop_tanggap');
            $table->string('p3k');
            $table->string('saluran');
            $table->string('open_dumping');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ketentuan_teknis');
    }
};
