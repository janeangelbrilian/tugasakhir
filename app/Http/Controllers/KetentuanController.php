<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Ketentuan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class KetentuanController extends Controller
{
    protected $status = null;
    protected $error = null;
    protected $data = null;

    public function index()
    {
        $ketentuan_teknis = Ketentuan::with('get_company')->get();
     

        $success =  $ketentuan_teknis;

        return response()->json([
            'success' => true,
            'message' => 'Ketentuan Teknis',
            'data' => $success
        ]);
    }

    public function store(Request $request){
        
  
        $data = $request->all();
        
        $user = Auth::user()->id;
        $validator = Validator::make($data, [
            // 'id_company' => 'required',
            'parameter' => 'required',
            'berkas' => 'mimes:pdf,jpg,png,jpeg|max:15000',
            'data' => []

        ]);

        print_r($request->input('papan'));
        exit();

        if ($validator->fails()){
            return response(
                [
                    'status' => "failed",
                    'data' => ["message" => "data salah"],
                    'error' => $validator->errors(),
                ]
                );
        }

        // $date = Carbon::now()->toDateString();
        $document = new Ketentuan();
        $document->id_company = $user;
        // $document->id_company = $request->id_company;
        $document->parameter = $request->parameter;
        // $document->latitude = $request->latitude;
        // $document->longitude = $request->longitude;
    if ($request->berkas && $request->berkas->isValid()) {
        $file_name = $request->berkas->getClientOriginalName();
        $request->berkas->move(public_path('ketentuan'), $file_name);
        $path = $file_name;
        $document->berkas = $path;
    } 
    try {
        $document->save();
        $this->data = $document;
        $this->status = "success";
    } catch (QueryException $e) {
        $this->status = "failed";
        $this->error = $e;
    }
    return response()->json([
        "status" => $this->status,
        "data" => $this->data,
        "error" =>$this->error
    ]);
    }
}
