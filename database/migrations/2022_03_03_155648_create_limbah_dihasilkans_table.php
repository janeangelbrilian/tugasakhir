<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('limbah_dihasilkans', function (Blueprint $table) {
            $table->id();
            $table->string('id_code');
            $table->string('id_company');
            $table->string('sumber');
            $table->string('jenis');
            $table->string('jml_timbulan');
            $table->string('kemasan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('limbah_dihasilkans');
    }
};
