<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_perusahaans', function (Blueprint $table) {
            $table->id();
            $table->string('nib');
            $table->string('nama');
            $table->string('surel');
            $table->string('telepon');
            $table->string('situs_web');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_perusahaans');
    }
};
