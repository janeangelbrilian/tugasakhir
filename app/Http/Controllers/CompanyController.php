<?php

namespace App\Http\Controllers;


use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{

    public function index()
    {
        
        return Company::when(request ('search'), function($query){
            $query->where('nama', 'like', '%' . request('search') . '%');
        })->orderBy('id', 'desc')->paginate(2);
  

        //get data from table posts
        // $companies = Company::latest()->get();

        //make response JSON
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Data Perusahaan',
        //     'data'    => $companies
        // ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */

    public function currentCompany(Request $request)
    {
        
        $companies= Company::with('get_company')->find($request->user());
        return response()->json($companies, 200);
    }

    public function show()
    {
        //find post by ID
        
        $companies = Company::findOrfail();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Perusahaam',
            'data'    => $companies
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nib'   => 'required',
            'tgl_terbit_nib'   => 'required',
            'nama' => 'required',
            'badan_hukum'   => 'required',
            'email'   => 'required',
            'password'   => 'required',
            'telepon' => 'required',
            'situs_web'   => 'required',
            'address' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $companies = Company::findOrFail($id);

        if ($companies) {

            //update post
            $companies->update([
                'nib'     => $request->nib,
                'tgl_terbit_nib'   => $request->tgl_terbit_nib,
                'nama'   => $request->nama,
                'badan_hukum'   => $request->badan_hukum,
                'email'     => $request->email,
                'password'     => $request->password,
                'telepon'   => $request->telepon,
                'situs_web'     => $request->situs_web,
                'address'   => $request->address
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Perusahaan Updated',
                'data'    => $companies
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Perusahaan Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $companies = Company::find($id);

        if ($companies) {

            //delete post
            $companies->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Perusahaan Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Perusahaan Not Found',
        ], 404);
    }
}
