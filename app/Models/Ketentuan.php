<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ketentuan extends Model
{
    use HasFactory;
     
    /**
     * filllable
     *
     * @var array
     */ 
    protected $fillable = [
        'parameter', 'berkas'
    ];
    public function get_company()
    {
        return $this->belongsTo(Company::class, 'id_company', 'id');
    }
}