<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Pesan;
use Illuminate\Database\QueryException;


class PesanController extends Controller
{
    
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'pesan'   => 'required',
            'tanggal' => 'required',
            'id_company' => 'required',
            // 'id_code' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $pesan = Pesan::create([
            'judul'   => $request->judul,
            'pesan'     => $request->pesan,
            'tanggal'   => $request->tanggal,
            'id_company' => $request->id_company,
            // 'id_code' => $request->id_code
        ]);

        //success save to database
        if ($pesan) {

            return response()->json([
                'success' => true,
                'message' => 'Pesan Created',
                'data'    => $pesan
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Pesan Failed to Save',
        ], 409);
    }

    public function index()
    {
        // if(request ('search')) {

        //     return Pesan::where('judul', 'like', '%' . request('search') . '%') ->orderBy('id', 'desc')->get();


        // }else {

        //     return Pesan::orderBy('id', 'desc')->with('get_company')->get();

        // }

            // $messages = Pesan::query();
            // if(request ('search')) {

            //     return $messages->where('judul', 'like', '%' . request('search') . '%') ->orderBy('id', 'desc')->get();
    
    
            // }else {
    
            //     return  $messages->orderBy('id', 'desc')->with('get_company')->get();
    
            // }

            return Pesan::when(request ('search'), function($query){
                $query->where('judul', 'like', '%' . request('search') . '%');
            })->orderBy('id', 'desc')->with('get_company')->paginate(2);
      
       
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $pesan = Pesan::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Pesan',
            'data'    => $pesan
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'pesan'   => 'required',
            'tanggal' => 'required',
            'id_company' => 'required',
            // 'id_code' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $pesan = Pesan::findOrFail($id);

        if ($pesan) {

            //update post
            $pesan->update([
                'judul'   => $request->judul,
                'pesan'     => $request->pesan,
                'tanggal'   => $request->tanggal,
                'id_company' => $request->id_company,
                // 'id_code' => $request->id_code
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Pesan Updated',
                'data'    => $pesan
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Pesan Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $pesan = Pesan::find($id);

        if ($pesan) {

            //delete post
            $pesan->delete();

            return response()->json([
                'success' => true,
                'message' => 'Pesan Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Pesan Not Found',
        ], 404);
    }

}