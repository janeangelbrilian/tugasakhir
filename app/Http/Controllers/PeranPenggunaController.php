<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\PeranPengguna;


class PeranPenggunaController extends Controller
{
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'priority'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $peran_pengguna = PeranPengguna::create([
            'name'     => $request->name,
            'priority'   => $request->priority,
        ]);

        //success save to database
        if ($peran_pengguna) {

            return response()->json([
                'success' => true,
                'message' => 'Daftar Peran Pengguna Created',
                'data'    => $peran_pengguna
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Daftar Peran Pengguna Failed to Save',
        ], 409);
    }

    public function index()
    {
        //get data from table posts
        $peran_pengguna = PeranPengguna::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Daftar Peran Pengguna',
            'data'    => $peran_pengguna
        ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $peran_pengguna = PeranPengguna::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Daftar Peran Pengguna',
            'data'    => $peran_pengguna
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'priority'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $peran_pengguna = PeranPengguna::findOrFail($id);

        if ($peran_pengguna) {

            //update post
            $peran_pengguna->update([
                'name'     => $request->name,
                'priority'   => $request->priority,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Peran Pengguna Updated',
                'data'    => $peran_pengguna
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Peran Pengguna Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $peran_pengguna = PeranPengguna::find($id);

        if ($peran_pengguna) {

            //delete post
            $peran_pengguna->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Peran Pengguna Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Peran Pengguna Not Found',
        ], 404);
    }

}