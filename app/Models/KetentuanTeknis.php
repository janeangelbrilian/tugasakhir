<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetentuanTeknis extends Model
{
    use HasFactory;
     
    /**
     * filllable
     *
     * @var array
     */ 
    protected $fillable = [
        'papan_nama','latitude', 'longitude', 'simbol_dan_label', 'pemisah', 'ventilasi', 'pallet', 'penerangan', 'apar', 'log_book', 'sop_penyimpanan', 'sop_tanggap', 'p3k', 'saluran', 'open_dumping'
    ];
    public function get_company()
    {
        return $this->belongsTo(Company::class, 'id_company', 'id');
    }
}
