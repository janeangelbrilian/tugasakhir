<?php

namespace App\Http\Controllers;


use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function index()
    {
        //get data from table posts
        $admins = Admin::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Admin',
            'data'    => $admins
        ], 200);
    }

    
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $admins = Admin::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Admin',
            'data'    => $admins
        ], 200);
    }
     /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email'   => 'required',
            'password' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $admins = Admin::findOrFail($id);

        if ($admins) {

            //update post
            $admins->update([
                'name'     => $request->name,
                'email'   => $request->email,
                'password'   => $request->password,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Daftar Admin Updated',
                'data'    => $admins
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Admin Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $admins = Admin::find($id);

        if ($admins) {

            //delete post
            $admins->delete();

            return response()->json([
                'success' => true,
                'message' => 'Daftar Admin Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Daftar Admin Not Found',
        ], 404);
    }
}
